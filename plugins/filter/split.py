#!/usr/bin/python

def split (value, index, char = ','):
    return value.split(char)[index]


class FilterModule(object):


    def filters(self):
        return {
            'split': split
        }